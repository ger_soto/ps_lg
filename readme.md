# PS LG


This project will resolve the code test proposed by DEVO to German Alvarez in July 2018

  - Python code golf.
  - Beautiful javascript charts.

# Python Code Golf

Checkout code and run as a shell script:

```sh
./Ejercicio1/ejercicio1.py
```

Before running, please make sure it is executable:

```sh
chmod +x ./Ejercicio1/ejercicio1.py
```

# Beautiful javascript charts

Before running it, please make sure you've the next software installed and configured in the computer
  - [Node.JS]
  - [npm] (Already included in Node.JS bundle)
  - [Bower]

The folder structure to run this project will be the next:
| Folder |   |
| ------------- | ------------- |
| `ejercicio2/galDevo` |Infrastructure folder. Here you may run npm in order to prepare and start the server |
| `ejercicio2/galDevo/app` |Served folder from the webserver |

In order to install and run the server, please move to the next folder (starting from git cloned folder root)
```sh
$ cd ejercicio2/galDevo
```

Afterwards, in order to run it, first install the dependencies by running npm:
```sh
$ npm install
```

And, when everything is installed, you are able to start the local webserver at port 8000
```sh
$ npm start
```

If the previous steps have finished successfully, please navigate to the webpage using your preferred browser:
```sh
127.0.0.1:8000
```

# Credits
Template created using [Dillinger]


[Dillinger]: https://dillinger.io/
[Node.JS]: https://nodejs.org/es/
[npm]: https://nodejs.org/es/
[Bower]: https://bower.io/
