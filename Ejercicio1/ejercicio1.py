#!/usr/bin/python

def divisores(numero, divisores = [], suma = 0):
    try:
        numero = int(numero);
    except:
        return -1;

    numerin = numero / 2
    tope = numero;
    divisores.append(1);
    suma = 0;
    for i in range(2,numerin):
        if i > tope or i == tope:
            break
        if numero%i == 0:
            divisores.append(i);
            divisores.append(numero/i);
            tope = numero/i;
            suma = sum(divisores)
        if suma > numero:
            # print "numero: ", numero, "suma: ", suma
            # print divisores;
            return 100;
    if suma == numero:
        # print "numero: ", numero, "suma: ", suma
        # print divisores;
        return 10;
    else:
        # print "numero: ", numero, "suma: ", suma
        # print divisores;
        return 0;



try:
    number = raw_input("Introduzca el numero: ");
    resultado = divisores( numero = number);
except:
    resultado = -1;


if resultado == -1:
    print "La entrada es invalida";
elif resultado == 0:
    print "Numero defectivo";
elif resultado == 10:
    print "Numero perfecto";
elif resultado == 100:
    print "Numero abundante";
