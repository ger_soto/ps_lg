'use strict';

// Declare app level module which depends on views, and components
angular.module('galDevo', [
  'galDevo.common',
  'ngRoute',
  'galDevoPage',
  'galDevo.main',
  'ui.bootstrap',
  'galDevo.hightcharts',
  'galDevo.fileSelector'
]).
config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({redirectTo: '/main'});
}]);
