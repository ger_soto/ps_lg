(function() {
  'use strict';

  angular
    .module('galDevo.hightcharts')
    .controller('GalDevoHcLineBasicCtrl', GalDevoHcLineBasicCtrl);

  GalDevoHcLineBasicCtrl.$inject = ['$element'];

  /* @ngInject */
  function GalDevoHcLineBasicCtrl($element) {
    var vm = this;
    vm.chartId = null;
    vm.chart = null;
    vm.$onChanges = _$onChanges;
    vm.$element = $element;
    vm.previousData = {};
    vm.hcOptions = {
      title: {},
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle'
      },
      plotOptions: {
        series: {
          label: {
            connectorAllowed: false
          },
          pointStart: 0,
          pointInterval : 86400 * 1000
        }
      },
      series: [],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 500
          },
          chartOptions: {
            legend: {
              layout: 'horizontal',
              align: 'center',
              verticalAlign: 'bottom'
            }
          }
        }]
      },
      xAxis: [{
        type:'datetime',
        startOnTick: false,
        endOnTick: false,
        labels: {
          formatter: function () {
            return Highcharts.dateFormat('%e. %b', this.value);
          }
        },
        tickInterval: 86400 * 1000 * 2,
      }]
    };

    var renderHc = _renderHc;
    var updateHc = _updateHc;

    function _$onChanges ( changes ) {
      console.log('GalDevoHcLineBasicCtrl $onChanges');
      updateHc();
    }

    function _updateHc () {
      renderHc();

      vm.hcOptions.title.text =  vm.title?vm.title:"";
      vm.hcOptions.series = [];
      vm.chart.update(vm.hcOptions);
      while (vm.chart.series.length > 0) {
        vm.chart.series[0].remove(true);
      }

      if ( !vm.data || !vm.data.series || vm.data.series.length == 0 ) {
        return;
      } else {
        for (var key in vm.data.series) {
          if (vm.data.series.hasOwnProperty(key)) {
            var seriesItem = {
              name : vm.data.series[key].name,
              data : vm.data.series[key].values,
            };
            vm.chart.addSeries(seriesItem);
          }
        }

        if (vm.data.xAxis && vm.data.xAxis[0]) {
          vm.hcOptions.plotOptions.series.pointStart = vm.data.xAxis[0];
        } else {
          vm.hcOptions.plotOptions.series.pointStart = 0;
        }
        vm.chart.update(vm.hcOptions);
      }
    }

    function _renderHc () {
      if (vm.chartId && vm.chart) {
        return;
      }
      vm.chartId = 'line-basic-placeholder' + Date.now();
      var placeHolder = vm.$element.find('div');
      placeHolder.attr("id",vm.chartId);
      vm.hcOptions.title.text = vm.title?vm.title:"";
      vm.chart = Highcharts.chart(vm.chartId, vm.hcOptions);
    }
  }
})();
