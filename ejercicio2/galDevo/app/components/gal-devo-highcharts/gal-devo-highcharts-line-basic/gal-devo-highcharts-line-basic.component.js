(function() {
    'use strict';

    angular
        .module('galDevo.hightcharts')
        .component('galDevoHcLineBasic', galDevoHcLineBasic());

    /* @ngInject */
    function galDevoHcLineBasic() {
        var component = {
            templateUrl: 'components/gal-devo-highcharts/gal-devo-highcharts-line-basic/gal-devo-highcharts-line-basic.html',
            controller: 'GalDevoHcLineBasicCtrl',
            bindings: {
              data: '<',
              title: '<',
              config: '<'
            }
        };

        return component;
    }
})();
