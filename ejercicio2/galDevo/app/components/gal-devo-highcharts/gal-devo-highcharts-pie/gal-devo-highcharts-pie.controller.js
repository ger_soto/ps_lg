(function() {
  'use strict';

  angular
    .module('galDevo.hightcharts')
    .controller('GalDevoHcPieCtrl', GalDevoHcPieCtrl);

  GalDevoHcPieCtrl.$inject = ['$element'];

  /* @ngInject */
  function GalDevoHcPieCtrl($element) {
    var vm = this;
    vm.chartId = null;
    vm.chart = null;
    vm.$onChanges = _$onChanges;
    vm.$element = $element;
    vm.previousData = {};
    vm.hcOptions = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
          text: ''
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.y:.1f}</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
              enabled: true,
              format: '<b>{point.name}</b>: {point.percentage:.1f} %',
              style: {
                  color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
              }
          }
        }
      },
      series: []
    };

    var renderHc = _renderHc;
    var updateHc = _updateHc;


    function _$onChanges(changes) {
      console.log('GalDevoHcPieCtrl $onChanges');
      updateHc();
    }


    function _updateHc() {
      renderHc();

      vm.hcOptions.title.text = vm.title ? vm.title : "";
      vm.hcOptions.series = [];
      vm.chart.update(vm.hcOptions);

      while (vm.chart.series.length > 0) {
        vm.chart.series[0].remove(true);
      }
      if (!vm.data || !vm.data.series || vm.data.series.length == 0) {
        return;
      } else {
        for (var key in vm.data.series) {
          if (vm.data.series.hasOwnProperty(key)) {
            vm.chart.addSeries(vm.data.series[key]);
          }
        }
      }
    }

    function _renderHc() {
      if (vm.chartId && vm.chart) {
        return;
      }
      vm.chartId = 'pie-placeholder-' + Date.now();
      var placeHolder = vm.$element.find('div');
      placeHolder.attr("id", vm.chartId);
      vm.hcOptions.title.text = vm.title?vm.title:"";
      vm.chart = Highcharts.chart(vm.chartId, vm.hcOptions);
    }
  }
})();
