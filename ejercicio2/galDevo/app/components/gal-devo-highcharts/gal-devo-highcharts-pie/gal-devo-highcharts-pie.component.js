(function() {
    'use strict';

    angular
        .module('galDevo.hightcharts')
        .component('galDevoHcPie', galDevoHcPie());

    /* @ngInject */
    function galDevoHcPie() {
        var component = {
            templateUrl: 'components/gal-devo-highcharts/gal-devo-highcharts-pie/gal-devo-highcharts-pie.html',
            controller: 'GalDevoHcPieCtrl',
            bindings: {
              data: '<',
              title: '<',
              config: '<'
            }
        };

        return component;
    }
})();
