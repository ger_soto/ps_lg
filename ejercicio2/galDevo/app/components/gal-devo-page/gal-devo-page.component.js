(function() {
    'use strict';

    angular
        .module('galDevoPage')
        .component('galDevoPage', component());

    /* @ngInject */
    function component() {
        var component = {
            templateUrl: './components/gal-devo-page/gal-devo-page.html',
            controller: 'GalDevoPageCtrl',
        };

        return component;
    }
})();
