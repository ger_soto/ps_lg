(function() {
    'use strict';

    angular
        .module('galDevoPage')
        .component('galDevoPageHead', component());

    /* @ngInject */
    function component() {
        var component = {
            templateUrl: './components/gal-devo-page-head/gal-devo-page-head.html',
            controller: 'GalDevoPageHeadCtrl',
        };

        return component;
    }
})();
