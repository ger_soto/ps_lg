(function() {
    'use strict';

    angular
        .module('galDevo.fileSelector')
        .component('galDevoFileSelector', galDevoFileSelector());

    /* @ngInject */
    function galDevoFileSelector() {
        var component = {
            templateUrl: 'components/gal-devo-file-selector/gal-devo-file-selector.html',
            controller: 'GalDevoFileSelectorCtrl',
            bindings : {
              files : '<',
              onFilesChanged : '&'
            }
        };

        return component;
    }
})();
