(function() {
    'use strict';

    angular
        .module('galDevo.fileSelector')
        .controller('GalDevoFileSelectorCtrl', GalDevoFileSelectorCtrl);

    GalDevoFileSelectorCtrl.$inject = [];

    /* @ngInject */
    function GalDevoFileSelectorCtrl() {
        var vm = this;
        vm.$onInit = _$onInit;
        vm.doOnChecked = _$doOnChecked;

        function _$onInit () {
          console.log('GalDevoFileSelectorCtrl $onInit');
          vm.data = angular.copy(vm.data);
        }

        function _$doOnChecked ( files ) {
          vm.onFilesChanged({ $event : { files: files }});
        }
    }
})();
