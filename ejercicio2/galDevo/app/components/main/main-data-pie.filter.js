(function() {
  'use strict';

  angular
    .module('galDevo.main')
    .filter('mainDataPieFilter', mainDataPieFilter);

  function mainDataPieFilter() {
    var vm = {};
    vm.transformDataPie = _transformDataPie;
    vm.processInputData = _processInputData;

    return mainDataPieFilterFilter;

    function mainDataPieFilterFilter(params) {
      vm.config = params.config;
      vm.outputData = { series : [] };
      return vm.transformDataPie(params.inputData);
    }


    function _transformDataPie(inputData) {

      if (!(inputData && inputData.length > 0)) {
        return vm.outputData;
      }

      var inputDataInternal = angular.copy(inputData);
      inputData.forEach(vm.processInputData);
      return vm.outputData;
    }

    function _processInputData(item) {
      if (vm.config.value && item[vm.config.value] && (typeof item[vm.config.value] === 'number')) {
        if (!vm.outputData.series || !vm.outputData.series[0]) {
          vm.outputData.series = [{
            name: vm.config.name,
            colorByPoint: true,
            data: []
          }];
        }

        var exists = false;
        for (var i in vm.outputData.series[0].data) {
          if (vm.outputData.series[0].data[i].name === item[vm.config.series]) {
            exists = true;
            vm.outputData.series[0].data[i].y += item[vm.config.value];
            break;
          }
        }
        if (exists === false) {
          vm.outputData.series[0].data.push({
            name: item[vm.config.series],
            y: item[vm.config.value]
          });
        }
      }
    }
  }
})();
