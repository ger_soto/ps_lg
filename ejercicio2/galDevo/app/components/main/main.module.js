'use strict';

angular.module('galDevo.main', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/main', {
    template: '<main>',
    controller: 'MainCtrl'
  });
}]);
