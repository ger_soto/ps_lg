(function() {
    'use strict';

    angular
        .module('galDevo.main')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = ['$filter', '$q', 'commonService'];

    /* @ngInject */
    function MainCtrl($filter, $q, commonService) {
        var vm = this;
        vm.$onInit = _$onInit;
        vm.onFilesChanged = _$onFilesChanged;

        var getFile = _getFile;
        var getData = _getData;
        var processDataToCharts = _processDataToCharts;

        vm.dataLineBasicConfiguration = {
          series: 'cat',
          xAxis: 'milliseconds',
          value: 'value'
        };
        vm.dataPieConfiguration = {
          series: 'cat', // Line
          xAxis: 'milliseconds', // line
          value: 'value', // line
          yAxis: 'cat', // Pie
          name: 'Total'
        };

        function _$onInit () {
          console.log('MainCtrl $onInit');
          vm.files = [{
            name: 'data1.json',
            status: true
          }, {
            name: 'data2.json',
            status: true
          }, {
            name: 'data3.json',
            status: true
          }];

          vm.data = [];
          processDataToCharts();
          getData(vm.files);
        }

        function _$onFilesChanged(inputData) {
          console.log('MainCtrl $onFilesChanged: ' + inputData);
          vm.files = inputData.files?inputData.files:vm.files;
          getData(vm.files);
        }

        function _getFile (filename) {
          return commonService.retrieveFile(filename);
        }

        function _getData(filenameArray) {
          vm.data = [];
          if (!(filenameArray && filenameArray.length >= 0)) {
            processDataToCharts()
            return;
          }

          var promiseArray = [];
          filenameArray.forEach(function (element) {
            if (element.status === true) {
              promiseArray.push(getFile(element.name));
            }

          });

          if (promiseArray.length === 0) {
            vm.data = [];
            processDataToCharts();
            return;
          }

          $q.all(promiseArray).then( function (responseArray) {
            responseArray.forEach( function (response) {
              response = $filter('dataFilter')(response);
              if (response) {
                response.forEach( function (element) {
                  if ( element && element.cat && element.milliseconds && element.value ) {
                    vm.data.push(element);
                  }
                });
              }
            });
          }, function (error) {}
        ).finally( function () {
            processDataToCharts();
          });
        }

        function _processDataToCharts () {
          vm.dataLineBasic = $filter('mainDataLineBasicFilter')({
            inputData : vm.data,
            config: vm.dataLineBasicConfiguration
          });
          vm.dataPie = $filter('mainDataPieFilter')({
            inputData: vm.data,
            config: vm.dataPieConfiguration
          });
        }
    }
})();
