(function() {
    'use strict';

    angular
        .module('galDevo.main')
        .filter('mainDataLineBasicFilter', mainDataLineBasicFilter);

    function mainDataLineBasicFilter() {
        var vm = {};
        vm.transformDataLineBasic = _transformDataLineBasic;
        vm.getDateArray = _getDateArray;
        vm.processInputData = _processInputData;

        return mainDataLineBasicFilterFilter;

        function mainDataLineBasicFilterFilter(params) {
          vm.config = params.config;
          vm.outputData = {};
          vm.dateArray = [];
          return vm.transformDataLineBasic (params.inputData);
        }

        function _transformDataLineBasic (inputData) {

          if ( !(inputData && inputData.length > 0) ) {
            return vm.outputData;
          }


          var inputDataInternal = angular.copy(inputData).sort( function (el1, el2) {
            return el1[vm.config.xAxis]-el2[vm.config.xAxis];
          });

          vm.dateArray = vm.getDateArray(inputDataInternal[0][vm.config.xAxis], inputDataInternal[inputData.length-1][vm.config.xAxis]);


          vm.outputData.xAxis = vm.dateArray;
          vm.outputData.series = {};
          inputDataInternal.forEach(vm.processInputData);
          return vm.outputData;
        }

        function _getDateArray (start, end) {

          var arr = [],
            dt = new Date(start);

          while (dt <= end) {
            var date = Date(dt);
            arr.push(Date.parse(dt));
            dt.setDate(dt.getDate() + 1);
          }
          return arr;
        }

        function _processInputData (item) {
          if ( vm.config.value && item[vm.config.value] && (typeof item[vm.config.value] === 'number') ) {

            if ( !vm.outputData.series[item[vm.config.series]] ) {
              vm.outputData.series[item[vm.config.series]] = {
                name : item[vm.config.series],
                values : vm.dateArray.map( function () { return 0.0; })
              };
            }

            var position = vm.outputData.xAxis.indexOf(item[vm.config.xAxis]);
            vm.outputData.series[item[vm.config.series]].values[position] += item[vm.config.value];
          }
        }
    }
})();
