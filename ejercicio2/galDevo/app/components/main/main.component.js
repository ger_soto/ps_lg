(function() {
    'use strict';

    angular
        .module('galDevo.main')
        .component('main', main());

    /* @ngInject */
    function main() {
        var component = {
            templateUrl: 'components/main/main.html',
            controller: 'MainCtrl',
        };

        return component;
    }
})();
