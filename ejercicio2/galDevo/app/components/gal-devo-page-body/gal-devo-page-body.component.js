(function() {
    'use strict';

    angular
        .module('galDevoPage')
        .component('galDevoPageBody', component());

    /* @ngInject */
    function component() {
        var component = {
            templateUrl: './components/gal-devo-page-body/gal-devo-page-body.html',
            controller: 'GalDevoPageBodyCtrl',
        };

        return component;
    }
})();
