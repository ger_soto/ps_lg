(function() {
    'use strict';

    angular
        .module('galDevo.common')
        .filter('dataFilterRaw', dataFilterRaw);

    function dataFilterRaw() {
        return dataFilterRawFilter;

        function dataFilterRawFilter (el) {
          var newEl = {};
          newEl.source = angular.copy(el);

          var regexPatt = /^(?:.*)((?:(?:(?:19|20)\d\d)(?:-)(?:09|04|06|11)(?:-)(?:0[1-9]|[12]\d|30))|(?:(?:(?:19|20)\d\d)(?:-)(?:01|03|05|07|08|10|12)(?:-)(?:0[1-9]|[12]\d|3[01]))|(?:(?:(?:(?:19|20)\d\d)(?:-)02(?:-)(?:0[1-9]|1\d|2[0-8]))|(?:(?:(?:(?:19|20)(?:04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96))|2000)(?:-)02(?:-)(?:29)))).*(?:#)(.*)(?:#)(?:.*)$/;
          var result = regexPatt.exec(el.raw);

          newEl.cat = result[2].toLowerCase();
          newEl.milliseconds = Date.parse(result[1]);
          newEl.value = el.val;
          return newEl;
        }
    }
})();
