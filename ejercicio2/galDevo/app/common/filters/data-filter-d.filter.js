(function() {
    'use strict';

    angular
        .module('galDevo.common')
        .filter('dataFilterD', dataFilterD);

    function dataFilterD() {
        return dataFilterDFilter;

        function dataFilterDFilter (el) {
          var newEl = {};
          newEl.source = angular.copy(el);
          newEl.cat = el.cat.toLowerCase();
          newEl.milliseconds = el.d;
          newEl.value = el.value;
          return newEl;
        }
    }
})();
