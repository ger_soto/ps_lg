(function() {
    'use strict';

    angular
        .module('galDevo.common')
        .filter('dataFilter', dataFilter);

    dataFilter.$inject = ['$filter'];

    function dataFilter($filter) {

        return dataFilterFilter;

        function dataFilterFilter(params) {
            params = params.map( function (element) {
              if ( element && element.d ) {
                return $filter('dataFilterD')(element);
              } else if ( element && element.myDate) {
                return $filter('dataFilterMyDate')(element);
              } else if ( element && element.raw ) {
                return $filter('dataFilterRaw')(element);
              }
            });
            return params;
        }
    }
})();
