(function() {
    'use strict';

    angular
        .module('galDevo.common')
        .filter('dataFilterMyDate', dataFilterMyDate);

    function dataFilterMyDate() {
        return dataFilterMyDateFilter;

        function dataFilterMyDateFilter (el) {
          var newEl = {};
          newEl.source = angular.copy(el);
          newEl.cat = el.categ.toLowerCase();
          newEl.milliseconds = Date.parse(el.myDate);
          newEl.value = el.val;
          return newEl;
        }
    }
})();
