(function() {
    'use strict';

    angular
        .module('galDevo.common')
        .factory('commonService', commonService);

    commonService.$inject = ['$http'];

    /* @ngInject */
    function commonService($http) {
        var service = {
            retrieveFile: retrieveFile
        };

        return service;

        function retrieveFile(filename) {
          return $http.get('data/' + filename).then(function (response) {
            if ( response && response.data ){
              return response.data;
            } else {
              return [];
            }
          }, function (error) {
            return [];
          });
        }
    }
})();
